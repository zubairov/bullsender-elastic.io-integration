var request = require('request');
var qs = require('querystring');

exports.getUserLists = function getUserLists(cfg, callback) {

    var queryUrl = 'https://api.bullsender.com/Lists/Get';

    var queryParams = {
        "api_key": cfg.api_key,
        "list_id": cfg.list
    };

    var options = {
        url: queryUrl + '?' + qs.stringify(queryParams),
        json: true
    };

    request.get(options, handleRequest);

    function handleRequest(error, response, body) {
        var result = {};
        if (response.statusCode == 200) {
            var lists = body.lists;
            var lc = lists.length;
            for (var i=0; i<lc; i++) {
                result[lists[i].id] = lists[i].name;
            }
        } else {
            // Nothing, we'll get a empty result
        }
        callback(null, result);
    }
};

exports.getListsCustomfields = function getListsCustomfields(cfg, callback) {

    // Let's fetch lists first
    getUserLists(cfg, function(err, lists) {
        if (err || Object.keys(lists).length === 0) {
            return cb('Can not find any suitable lists of error when fetching them');
        }
        // Now we have a lists and we can fetch metadata
        
        var out = require('../schemas/add_subscriber.out.json');

        var queryUrl = 'https://api.bullsender.com/Customfields/Get';

        var queryParams = {
            "api_key": cfg.api_key,
            "list_id": Object.keys(lists)[0]
        };

        var options = {
            url: queryUrl + '?' + qs.stringify(queryParams),
            json: true
        };

        request.get(options, handleRequest);

        function handleRequest(error, response, body) {
            if (error) return cb(error);
            
            var fields = {
                "email": {
                    "type": "string",
                    "title": "Email",
                    "id": "email",
                    "required": true
                }
            };
            if (response.statusCode == 200) {
                var cfs = body.customfields;

                for (var i in cfs) {
                    if (cfs.hasOwnProperty(i)) {
                        fields[cfs[i].id] = {
                            "type": "string", // We could make this dynamic, but for now we'll just use a string
                            "title": cfs[i].name,
                            "id": cfs[i].id,
                            "required": cfs[i].required
                        };
                    }
                }
            }
            var _in = {
                "type": "object",
                "properties": fields
            };
            callback(null, {
                "in": _in,
                "out": out
            });
        }

    });

};